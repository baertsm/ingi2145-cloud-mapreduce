\documentclass[a4paper,11pt]{article} % 11pt
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[english]{babel}
\usepackage[english]{varioref} % \vpageref

\usepackage[top=2cm, bottom=2cm, left=2cm, right=2cm]{geometry}

\usepackage{amsmath,amssymb,gensymb} % math
\usepackage{graphicx} % images
\usepackage{float}
\usepackage{caption}
% \usepackage{qtree}    % dessiner des arbres %% => texlive-humanities
\usepackage[pdfusetitle,hidelinks]{hyperref}
\usepackage{url}
\usepackage[usenames]{color}
\usepackage[backgroundcolor=yellow]{todonotes} %% todonotes: \listoftodos & \todo{Some note or other.} & \missingfigure{}
\usepackage{titling}
\IfFileExists{fourier.sty}{\usepackage{fourier}}{\typeout{! WARNING: Fourier package not included: skip it}}

\definecolor{codeBlue}{rgb}{0,0,1}
\definecolor{webred}{rgb}{0.5,0,0}
\definecolor{codeGreen}{rgb}{0,0.5,0}
\definecolor{codeGrey}{rgb}{0.6,0.6,0.6}
\definecolor{webdarkblue}{rgb}{0,0,0.4}
\definecolor{webgreen}{rgb}{0,0.3,0}
\definecolor{webblue}{rgb}{0,0,0.8}
\definecolor{orange}{rgb}{0.7,0.1,0.1}


% Renew
\renewcommand{\familydefault}{\sfdefault} % Font: SF
\urlstyle{sf}
\renewcommand{\labelitemi}{$\bullet$} % itemize: bullets

\usepackage{listings}		% Pour l'insersion de fichiers de codes sources.
\lstset{
	  language=Java,
	  flexiblecolumns=true,
	  numbers=left,
	  stepnumber=1,
	  numberstyle=\ttfamily\tiny,
	  keywordstyle=\ttfamily\textcolor{blue},
	  stringstyle=\ttfamily\textcolor{red},
	  commentstyle=\ttfamily\textcolor{green},
	  breaklines=true,
	  extendedchars=true,
	  basicstyle=\ttfamily\scriptsize,
	  showstringspaces=false
	}

%%%%%%%%%%%%%%%%%%%%

\setlength{\droptitle}{-2cm}
\title{LINGI2145 - Cloud Computing - Homework 1}
\author{Matthieu Baerts}
\date{November 2014}

\begin{document}

\maketitle
\vspace*{-1cm}

\section*{Introduction}
The goal of this first homework assignment was to implement an algorithm called \textit{SocialRank} by using Hadoop on AWS. This algorithm has to be used to find most influential persons in a social network like LiveJournal.

% A high­level description of your solution, and of the implementation choices you made
% In particular, you should explain the intermediate representation that you used.
\section{Description of the solution}
For this first homework, we received a skeleton that we had to respect. Our algorithm has to support different steps: \texttt{init}, \texttt{iter}, \texttt{diff}, \texttt{final} and \texttt{composite} which has to use all previous steps.

    \subsection{First steps: creating an intermediate format}
    The first important point is to choose a suitable intermediate format which will be created during the initial phase and used when starting all other steps. The formula of this algorithm to compute $r_i^{k+1}$, the rank of vertex $i$ at the iteration $k+1$ is the following one:
    
        \[ r_i^{k+1} = d + (1 - d) \sum_{j \in B(i)} \frac{r_j^k}{|N(j)|} \]

    Where $d$ is a constant ($0.15$ in our case), $B$ is a set of users which have $i$ as a \textit{friend} and $|N|$ is the number of \textit{friends} of $i$.

    When looking at the formula, we can see that only the rank of other nodes is variable. For each iteration and each vertex, we will need to keep the current rank which is variable and recompute it during each iteration. It can also be interesting to save the list of \textit{friends} ($N$) of the node in order to easily compute $B$ and $|N|$. Indeed we can compute that simply by mapping each \textit{friends} of a vertex as key, in order to get the set of $B$, with the value $\frac{r_j^k}{|N(j)|}$. By doing that, we will have all elements needed to compute the next rank. The selected intermediate format can be created from the initial format by using the vertex ID as the key and the rank and the \textit{friends} as the values and we can write all of that in this form:

        \[ \#\texttt{Vertex} \qquad \texttt{Rank} \qquad \#\texttt{friend1},\#\texttt{friend2},\texttt{...} \]

    Note that at the end of the \texttt{iter} step, we will also need to save data with the new rank in the same intermediate format. This is why the mapper of the \texttt{iter} step has to also send this original intermediate format to the reducers. In other words, it means that in the \texttt{reducer} method of the \texttt{iter} step and for each vertex, we will get all $\frac{r_j^k}{|N(j)|}$, from the set of $B$ to compute the new rank, and the list of friends of this node ($N$), to rebuild the intermediate format with this new rank.

    \subsection{Next steps}
    The steps \texttt{diff} and \texttt{final} are simpler to describe.
        \paragraph{\texttt{diff}} This step firstly takes the output of two different sources and computes the difference of the rank of both sources by grouping the same vertexes together with their rank. Then we can compute the maximum value by listing all of them and take the maximum one.
        
        Note that it seems the skeleton imposes us to use only one reducer to do this second part of this step because the method \texttt{readDiffResult} will read only one file. But it can be interesting to use more reducers to get the maximum of each list of values given to the reducers and then compute the maximum of all lists by looking at all files written in the output directory.
    
        \paragraph{\texttt{final}} The goal of this last step is to sort the list of vertexes by decreasing social rank and output it to a single file. Because we need a single file and in order to have a simple solution, this job will be made by only one reducer which will just write items which have been sorted by decreasing social rank.

% Describe the challenges you encountered, if any (it's fine not to have anything to say, don't make up something).
\section{Challenges}
The biggest challenges were certainly to find a suitable intermediate format to process an \texttt{iter} step and to find a good algorithm for \textit{MapReduce} jobs.

After that, it's interesting to see that it's sometimes easier to have an "\textit{ugly}" solution. Indeed, we could only use \texttt{String} (from a \texttt{Text} class) as keys and values that we will manipulate in all \texttt{map} and \texttt{reduce} methods. Of course, it possible to manipulate at the same time float numbers and lists of numbers written in strings but it's maybe cleaner to use the appropriate format when it's possible. E.g. we choose here to store the rank and the list of \textit{friends} in a specific object (\texttt{VertexInfoWritable}). In the \texttt{iter} step, the mapper will write two different formats for the values: one with just a float number (\texttt{DoubleWritable}) and another one with a list of \textit{friends} (\texttt{Text}). A generic class has been created to handle that and the different objects are detected in the reducer thank to \texttt{instanceof} keyword.

Note that we also try to optimise some details from part of our code which will be used a lot of time, e.g. by not duplicating all \texttt{String} in an array when we just need the first entry, by not creating an array when we don't need to manipulate it but just print it, etc. We also try to not add useless data, e.g. by not adding the number of \textit{friends} in the initial format --- we already have a list of friends that we will have to parse --- or by using \texttt{NullWritable} class instead of bigger objects which will not be used. We also assume that our intermediate files are not modified by external users in order to avoid some useless checks.


% Output of the top­10 ranked users in the format <user ID> <rank>
\section{Solution: Top 10}
Note: if the goal is to minimise the number of iterations, we could improve the results --- stopping the iterations just when the maximum difference between two iterations is lower than the threshold --- by increasing the number of use of the \texttt{diff} step in the \texttt{composite} method. We could also used \texttt{diff} more often when the previous maximum difference is closed to the threshold.

% Elastic MapReduce cluster ID
\begin{table}[H]\footnotesize
    \centering
    \begin{tabular}{|r|l|}
        \hline
        \textbf{Vertex} & \textbf{Rank} \\
        \hline
        852    & 1093.018 \\
        44129  & 943.183  \\
        30943  & 928.947  \\
        86152  & 859.054  \\
        3586   & 725.472  \\
        14277  & 634.292  \\
        509    & 372.201  \\
        242340 & 337.011  \\
        109482 & 304.260  \\
        54631  & 303.165  \\
        \hline
    \end{tabular}
    \caption{Top 10 from \textit{Elastic MapReduce} cluster ID \texttt{j-2LCGDNRE66MLA}}
    \label{tab:top10}
\end{table}


\section{Reciprocity}
To compute the reciprocity, we will need two jobs: one to get the number ($> 0$) of links between two nodes --- by using \texttt{lowestID:biggestID} as key --- and the other one to compute the percentage of links that are bidirectional. The reciprocity of the \textit{LiveJournal} seems to be : $0.574$. Note that the case where the link is unidirectional but there is a duplicated line is not managed.


\section*{Conclusion}
Thanks to this homework, we are now able to better understand how a \textit{MapReduce} algorithm is built.


\end{document}
