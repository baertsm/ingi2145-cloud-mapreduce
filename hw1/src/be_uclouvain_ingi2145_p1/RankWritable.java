package be_uclouvain_ingi2145_p1;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.WritableComparable;

/**
 * Vertex that is comparable by using the rank (DESC order)
 */
public class RankWritable implements WritableComparable<RankWritable> {

    private DoubleWritable rank = new DoubleWritable();

    public RankWritable() {
        super();
    }

    public RankWritable(double rank) {
        super();
        this.rank.set(rank);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        rank.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        rank.readFields(in);
    }

    @Override
    public int compareTo(RankWritable other) {
        return -rank.compareTo(other.rank);
    }

    @Override
    public int hashCode() {
        return rank.hashCode();
    }

    @Override
    public String toString() {
        return Double.toString(rank.get());
    }

    public DoubleWritable getRank() {
        return rank;
    }
}
