package be_uclouvain_ingi2145_p1;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * Map all current keys with only the rank (without the edges)
 */
public class DiffVertexMap extends
        Mapper<Text, Text, LongWritable, DoubleWritable> {
    @Override
    protected void map(Text key, Text value, Context context)
            throws IOException, InterruptedException {

        double rank = VertexInfoWritable.getRankFromValue(value.toString());

        context.write(new LongWritable(Long.parseLong(key.toString())),
                new DoubleWritable(rank));
    }
}
