package be_uclouvain_ingi2145_p1;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * Compute and write the difference between the two values of a key.
 */
public class DiffVertexReduce extends
        Reducer<LongWritable, DoubleWritable, LongWritable, DoubleWritable> {
    @Override
    protected void reduce(LongWritable key, Iterable<DoubleWritable> values,
            Context context) throws IOException, InterruptedException {

        double oldRank, newRank, diff;
        Iterator<DoubleWritable> iterator = values.iterator();

        oldRank = iterator.next().get();

        // it's possible to not have a second value e.g.: if it has no edges
        if (iterator.hasNext())
            newRank = iterator.next().get();
        else
            newRank = 0;

        diff = Math.abs(oldRank - newRank);

        context.write(key, new DoubleWritable(diff));
    }

}
