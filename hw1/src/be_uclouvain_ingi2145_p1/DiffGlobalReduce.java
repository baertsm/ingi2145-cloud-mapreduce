package be_uclouvain_ingi2145_p1;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * Get the maximum value of all ones.
 */
public class DiffGlobalReduce extends
        Reducer<NullWritable, DoubleWritable, DoubleWritable, NullWritable> {

    @Override
    protected void reduce(NullWritable key, Iterable<DoubleWritable> values,
            Context context) throws IOException, InterruptedException {

        double maxDiff = Double.MIN_VALUE;

        for (DoubleWritable valWritable : values) {
            double val = valWritable.get();
            if (val > maxDiff)
                maxDiff = val;
        }

        context.write(new DoubleWritable(maxDiff), NullWritable.get());
    }
}
