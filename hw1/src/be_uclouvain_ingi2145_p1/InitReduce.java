package be_uclouvain_ingi2145_p1;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * For each key (vertex), write the initial rank (1) with all edges
 */
public class InitReduce extends
        Reducer<LongWritable, LongWritable, LongWritable, VertexInfoWritable> {
    @Override
    protected void reduce(LongWritable key, Iterable<LongWritable> values,
            Context context) throws IOException, InterruptedException {

        /*
         * Could be better to create an array (LongArrayWritable extends
         * ArrayWritable) but we just need to get a string which represents the
         * array... No need to parse the text, build the array, then to string.
         */
        StringBuffer buffer = new StringBuffer();
        Iterator<LongWritable> iter = values.iterator();

        // tips to not add an extra SPLIT_CHAR
        if (iter.hasNext())
            buffer.append(iter.next().get());

        while (iter.hasNext()) {
            buffer.append(VertexInfoWritable.SPLIT_CHAR);
            buffer.append(iter.next().get());
        }

        VertexInfoWritable vertexInfo = new VertexInfoWritable(1.,
                buffer.toString());
        context.write(key, vertexInfo);
    }
}