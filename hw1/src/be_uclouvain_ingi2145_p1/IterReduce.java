package be_uclouvain_ingi2145_p1;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * Compute the new rank of a key and write the new rank with all its edges
 */
public class IterReduce extends
        Reducer<LongWritable, IterGenericClass, LongWritable, VertexInfoWritable> {
    @Override
    protected void reduce(LongWritable key, Iterable<IterGenericClass> values,
            Context context) throws IOException, InterruptedException {

        double newRank = SocialRankDriver.D;
        Text edges = null;

        for (IterGenericClass valueGeneric : values) {
            Writable value = valueGeneric.get();
            if (value instanceof Text) {
                edges = (Text) value;
            }
            else {
                DoubleWritable rank = (DoubleWritable) value;
                newRank += (1 - SocialRankDriver.D) * rank.get();
            }
        }

        context.write(key, new VertexInfoWritable(newRank, edges));
    }
}
