package be_uclouvain_ingi2145_p1;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.GenericWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

/**
 * We can have two classes in the reduce part of the iter operation:
 * VertexInfoWritable and DoubleWritable
 */
public class IterGenericClass extends GenericWritable {

    @SuppressWarnings("unchecked")
    private static Class<? extends Writable>[] CLASSES = (Class<? extends Writable>[]) new Class[] {
            Text.class, DoubleWritable.class };

    public IterGenericClass() {
    }

    public IterGenericClass(Writable instance) {
        set(instance);
    }

    @Override
    protected Class<? extends Writable>[] getTypes() {
        return CLASSES;
    }

}
