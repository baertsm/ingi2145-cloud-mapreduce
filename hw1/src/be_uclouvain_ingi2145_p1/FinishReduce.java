package be_uclouvain_ingi2145_p1;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * Just write the sorted results: vertex rank
 */
public class FinishReduce extends
        Reducer<RankWritable, Text, Text, DoubleWritable> {

    @Override
    protected void reduce(RankWritable key,
            Iterable<Text> values, Context context) throws IOException,
            InterruptedException {

        // We can have values with the same rank
        for (Text value : values) {
            context.write(value, key.getRank());
        }
    }
}
