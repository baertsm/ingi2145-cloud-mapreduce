package be_uclouvain_ingi2145_p1;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * Link all values to the same key (null)
 */
public class DiffGlobalMap extends
        Mapper<Text, Text, NullWritable, DoubleWritable> {
    @Override
    protected void map(Text key, Text value, Context context)
            throws IOException, InterruptedException {
        context.write(NullWritable.get(),
                new DoubleWritable(Double.parseDouble(value.toString())));
    }
}
