package be_uclouvain_ingi2145_p1;

import java.io.IOException;

import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * Map all items with values which should be [1,2] (but accept >2 ; not 0) and
 * converted to boolean (true == bidirectional)
 */
public class ReciprocityGlobalMap extends
        Mapper<Text, Text, NullWritable, BooleanWritable> {

    @Override
    protected void map(Text key, Text value, Context context)
            throws IOException, InterruptedException {

        /*
         * 'count' cannot be 0 but could be > 2 if we have duplicated lines.
         * Note: the case where the link is unidirectional but there is a
         * duplicated line is not managed (e.g. if we have 1 -> 2 ; 1 -> 2)
         */
        int count = Integer.parseInt(value.toString());
        boolean bidirectional = count > 1;

        context.write(NullWritable.get(), new BooleanWritable(bidirectional));
    }
}
