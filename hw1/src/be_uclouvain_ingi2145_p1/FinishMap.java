package be_uclouvain_ingi2145_p1;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * Map only with the key (vertex) and the rank and sort (DESC) it with the rank
 */
public class FinishMap extends Mapper<Text, Text, RankWritable, Text> {

    @Override
    protected void map(Text key, Text value, Context context)
            throws IOException, InterruptedException {

        double rank = VertexInfoWritable.getRankFromValue(value.toString());

        context.write(new RankWritable(rank), key);
    }
}
