package be_uclouvain_ingi2145_p1;

import java.io.IOException;

import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * Compute the percentage of values which are bidirectional (true)
 */
public class ReciprocityGlobalReduce extends
        Reducer<NullWritable, BooleanWritable, DoubleWritable, NullWritable> {

    @Override
    protected void reduce(NullWritable key, Iterable<BooleanWritable> values,
            Context context) throws IOException, InterruptedException {
        long total = 0, bidirectional = 0;

        for (BooleanWritable val : values) {
            if (val.get())
                bidirectional++;
            total++;
        }

        double reciprocity = bidirectional / (double) total;

        context.write(new DoubleWritable(reciprocity), NullWritable.get());
    }
}
