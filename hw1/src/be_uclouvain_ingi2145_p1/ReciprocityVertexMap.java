package be_uclouvain_ingi2145_p1;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * Map with the concatenation of 'key and value' or 'value and key' where the
 * first one is always the lowest one (to detect when we have key/value and
 * value/key)
 */
public class ReciprocityVertexMap extends
        Mapper<Text, Text, ReciprocityWritable, NullWritable> {

    @Override
    protected void map(Text key, Text value, Context context)
            throws IOException, InterruptedException {

        long keyLong = Long.parseLong(key.toString());
        long valueLong = Long.parseLong(value.toString());

        context.write(new ReciprocityWritable(keyLong, valueLong),
                NullWritable.get());
    }
}
