package be_uclouvain_ingi2145_p1;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * Map with the current key (we receive: "Key    Value")
 */
public class InitMap extends
 Mapper<Text, Text, LongWritable, LongWritable> {
    @Override
    protected void map(Text key, Text value, Context context)
            throws IOException, InterruptedException {
        context.write(new LongWritable(Long.parseLong(key.toString())),
                new LongWritable(Long.parseLong(value.toString())));
    }
}
