package be_uclouvain_ingi2145_p1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.varia.LevelRangeFilter;

public class SocialRankDriver extends Configured implements Tool {
    /**
     * Singleton instance.
     */
    public static SocialRankDriver GET;

    /**
     * Number of iterations to perform in between diff checks.
     */
    public static final double DIFF_INTERVAL = 3;

    /**
     * Constant used in Social Rank Algo
     */
    public static final double D = 0.15;

    /**
     * Split char used to separate columns in text files
     */
    public static final char SEP = '\t';
    public static final String SEPSTRING = Character.toString(SEP);

    /**
     * Author's name.
     */
    public static final String NAME = "Matthieu Baerts";

    // ---------------------------------------------------------------------------------------------

    public static void main(String[] args) throws Exception {
        // configure log4j to output to a file
        Logger logger = LogManager.getRootLogger();
        logger.addAppender(new FileAppender(new SimpleLayout(), "p1.log"));

        // configure log4j to output to the console
        Appender consoleAppender = new ConsoleAppender(new SimpleLayout());
        LevelRangeFilter filter = new LevelRangeFilter();
        /*
         * switch to another level for more detail
         * (own (INGI2145) messages use FATAL)
         */
        filter.setLevelMin(Level.FATAL);
        consoleAppender.addFilter(filter);
        // (un)comment to (un)mute console output
        logger.addAppender(consoleAppender);

        // switch to Level.DEBUG or Level.TRACE for more detail
        logger.setLevel(Level.INFO);

        GET = new SocialRankDriver();
        Configuration conf = new Configuration();
        conf.set(
                "mapreduce.input.keyvaluelinerecordreader.key.value.separator",
                SEPSTRING);

        int res = ToolRunner.run(conf, GET, args);
        System.exit(res);
    }

    // ---------------------------------------------------------------------------------------------

    @Override
    public int run(String[] args) throws Exception {
        System.out.println(NAME);

        if (args.length == 0) {
            args = new String[] { "command missing" };
        }

        switch (args[0]) {
        case "init":
            init(args[1], args[2], Integer.parseInt(args[3]));
            break;
        case "iter":
            iter(args[1], args[2], Integer.parseInt(args[3]));
            break;
        case "diff":
            diff(args[1], args[2], args[3], args[4], Integer.parseInt(args[5]));
            break;
        case "finish":
            finish(args[1], args[2], args[3], Integer.parseInt(args[4]));
            break;
        case "composite":
            composite(args[1], args[2], args[3], args[4], args[5], args[6],
                    Double.parseDouble(args[7]), Integer.parseInt(args[8]));
            break;
        case "reciprocity":
            reciprocity(args[1], args[2], args[3], Integer.parseInt(args[4]));
            break;
        default:
            System.out.println("Unknown command: " + args[0]);
            break;
        }

        return 0;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * This job reads the file(s) in the input directory, convert them into an
     * intermediate format, and output the data to the output directory, using
     * the specified number of reducers.
     */
    private void init(String inputDir, String outputDir, int nReducers)
            throws Exception {
        Logger.getRootLogger().fatal("[INGI2145] init");

        Job init = Utils.configureJob(inputDir, outputDir, InitMap.class, null,
                InitReduce.class, LongWritable.class, LongWritable.class,
                LongWritable.class, VertexInfoWritable.class, nReducers);
        init.setInputFormatClass(KeyValueTextInputFormat.class);
        Utils.startJob(init);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * This job performs a single round of SocialRank by reading data in an
     * intermediate format from the input directory, and writing data in an
     * intermediate format to the output directory, using the specified number
     * of reducers.
     */
    private void iter(String inputDir, String outputDir, int nReducers)
            throws Exception {
        Logger.getRootLogger().fatal(
                "[INGI2145] iter: " + inputDir + " (to) " + outputDir);

        Job iter = Utils.configureJob(inputDir, outputDir, IterMap.class, null,
                IterReduce.class, LongWritable.class, IterGenericClass.class,
                LongWritable.class, VertexInfoWritable.class, nReducers);
        Utils.startJob(iter);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * This job reads data in the intermediate format from both input
     * directories and output a single line that contains the maximum difference
     * between any pair of rank values for the same vertex. Absolute values are
     * used for the differences, i.e., the change from 0.98 to 0.97 is 0.01.
     * While diff runs as a single task, two different MapReduce jobs will run
     * successively. In that case, <tmpDir> is used as output for the first job
     * and input to the second job.
     */
    private double diff(String inputDir1, String inputDir2, String tmpDir,
            String outputDir, int nReducers) throws Exception {
        Logger.getRootLogger().fatal("[INGI2145] diff");

        // each vertex: compute the difference
        Job diffVertex = Utils.configureJob(inputDir1, tmpDir,
                DiffVertexMap.class, null, DiffVertexReduce.class,
                LongWritable.class, DoubleWritable.class, LongWritable.class,
                DoubleWritable.class, nReducers);
        FileInputFormat.addInputPath(diffVertex, new Path(inputDir2));
        diffVertex.setInputFormatClass(KeyValueTextInputFormat.class);
        Utils.startJob(diffVertex);

        // regroup all values: get the max one
        Job diffGlobal = Utils.configureJob(tmpDir, outputDir,
                DiffGlobalMap.class, null, DiffGlobalReduce.class,
                NullWritable.class, DoubleWritable.class, DoubleWritable.class,
                NullWritable.class, 1); // max one key, one task!
        diffGlobal.setInputFormatClass(KeyValueTextInputFormat.class);
        Utils.startJob(diffGlobal);

        double diffResult = Utils.readDiffResult(outputDir);
        Logger.getRootLogger().fatal("[INGI2145] diff was " + diffResult);
        return diffResult;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * This job reads data in an intermediate format from the input directory,
     * convert the data to the output format, sort it by decreasing social rank
     * and output it to the a single file in the output directory, using the
     * specified number of reducers (whenever possible). While finish runs as a
     * single task, two different MapReduce jobs will run successively. In that
     * case, <tmpDir> is used as output for the first job and input to the
     * second job.
     */
    private void finish(String inputDir, String tmpDir, String outputDir,
            int nReducers) throws Exception {
        Logger.getRootLogger().fatal("[INGI2145] finish from:" + inputDir);

        Job finish = Utils.configureJob(inputDir, outputDir, FinishMap.class,
                null, FinishReduce.class, RankWritable.class, Text.class,
                Text.class, DoubleWritable.class, 1); // sort, just use 1 task!
        finish.setInputFormatClass(KeyValueTextInputFormat.class);
        Utils.startJob(finish);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * This function is submitted only once and runs the entire SocialRank
     * algorithm from beginning to end, i.e., until convergence has occurred. It
     * runs the init task, then it alternate between running the iter and diff
     * tasks until convergence has occurred. The difference threshold is given
     * by the <delta> parameter.
     */
    private void composite(String inputDir, String outputDir,
            String intermDir1, String intermDir2, String diffDir,
            String tmpDiffDir, double delta, int nReducers) throws Exception {

        // Init:
        init(inputDir, intermDir1, nReducers);
        
        // Iter + Diff:
        double maxDiff = Double.MAX_VALUE;
        int i = 0;
        do {
            // use the right dir
            if (i % 2 == 0)
                iter(intermDir1, intermDir2, nReducers);
            else
                iter(intermDir2, intermDir1, nReducers);

            // Advised to run only each 2 or 3 iterations
            if (i > 0 && i % 3 == 0)
                maxDiff = diff(intermDir1, intermDir2, tmpDiffDir, diffDir,
                    nReducers);
            i++;
        } while (maxDiff > delta);

        // Finish:
        if (i % 2 == 0)
            finish(intermDir1, intermDir2, outputDir, nReducers);
        else
            finish(intermDir2, intermDir1, outputDir, nReducers);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Calculate the reciprocity in LiveJournal, i.e., the fraction of links
     * that are bidirectional (A is a friend of B and B is a friend of A). Note:
     * bidirectional links are counted only once:
     *     1 -> 2, 1 -> 3, 2 -> 1: reciprocity is 1/2
     */
    private double reciprocity(String inputDir, String tmpDir,
            String outputDir, int nReducers) throws Exception {
        Logger.getRootLogger().fatal("[INGI2145] reciprocity");

        // each vertex: get all links
        Job reciprocityVertex = Utils.configureJob(inputDir, tmpDir,
                ReciprocityVertexMap.class, null,
                ReciprocityVertexReduce.class, ReciprocityWritable.class,
                NullWritable.class, ReciprocityWritable.class,
                IntWritable.class, nReducers);
        reciprocityVertex.setInputFormatClass(KeyValueTextInputFormat.class);
        Utils.startJob(reciprocityVertex);

        // regroup all values: get the reciprocity
        Job reciprocityGlobal = Utils.configureJob(tmpDir, outputDir,
                ReciprocityGlobalMap.class, null,
                ReciprocityGlobalReduce.class, NullWritable.class,
                BooleanWritable.class, DoubleWritable.class,
                NullWritable.class, 1); // max one key: one task
        reciprocityGlobal.setInputFormatClass(KeyValueTextInputFormat.class);
        Utils.startJob(reciprocityGlobal);

        double reciprocity = Utils.readDiffResult(outputDir);
        Logger.getRootLogger()
                .fatal("[INGI2145] reciprocity is " + reciprocity);
        return reciprocity;
    }
}