package be_uclouvain_ingi2145_p1;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

/**
 * Contains a rank and a list of edges saved in a string
 */
public class VertexInfoWritable implements Writable {

    public static final char SPLIT_CHAR = ',';
    public static final String SPLIT = Character.toString(SPLIT_CHAR);

    private DoubleWritable rank = new DoubleWritable();
    private Text edges;

    public VertexInfoWritable() {
        super();
        this.edges = new Text();
    }

    public VertexInfoWritable(double rank, String edges) {
        super();
        this.rank.set(rank);
        if (edges != null)
            this.edges = new Text(edges);
        else
            this.edges = new Text();
    }

    public VertexInfoWritable(double rank, Text edges) {
        super();
        this.rank.set(rank);
        if (edges != null)
            this.edges = edges;
        else
            this.edges = new Text();
    }

    /**
     * Set the new rank
     *
     * @param rank
     *            will replace the old one
     */
    public void setRank(double rank) {
        this.rank.set(rank);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        rank.write(out);
        edges.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        rank.readFields(in);
        edges.readFields(in);
    }

    @Override
    public String toString() {
        return rank.get() + SocialRankDriver.SEPSTRING + edges.toString();
    }

    /**
     * Get an array of edges from a string
     *
     * @param edgesString
     *            contains all edges separated by the {@value #SPLIT} char
     * @return array of edges from a string
     */
    public static LongWritable[] getEdges(String edgesString) {
        String[] edgesArray = edgesString.split(SPLIT);
        LongWritable[] edges = new LongWritable[edgesArray.length];
        for (int i = 0; i < edgesArray.length; i++) {
            long vertex = Long.parseLong(edgesArray[i]);
            edges[i] = new LongWritable(vertex);
        }
        return edges;
    }

    /**
     * Sometime, we just need the rank, first entry, not the edges after '\t'
     *
     * @param value
     *            contains the rank and edges: Rank edge1,edge2,edge3,...
     * @return rank parsed from the string
     */
    public static double getRankFromValue(String value) {
        String rank = value.substring(0, value.indexOf(SocialRankDriver.SEP));
        return Double.parseDouble(rank);
    }
}
