package be_uclouvain_ingi2145_p1;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.WritableComparable;

/**
 * Used to compare/hash both vertex and edges
 */
public class ReciprocityWritable implements
        WritableComparable<ReciprocityWritable> {

    private LongWritable vertex = new LongWritable();
    private LongWritable edge = new LongWritable();

    public ReciprocityWritable() {
        super();
    }

    /**
     * The vertex will be the lower id.
     */
    public ReciprocityWritable(long id1, long id2) {
        super();
        if (id1 < id2) {
            this.vertex.set(id1);
            this.edge.set(id2);
        }
        else {
            this.vertex.set(id2);
            this.edge.set(id1);
        }
    }

    @Override
    public void write(DataOutput out) throws IOException {
        vertex.write(out);
        edge.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        vertex.readFields(in);
        edge.readFields(in);
    }

    @Override
    public int compareTo(ReciprocityWritable other) {
        int vertexComp = vertex.compareTo(other.vertex);
        if (vertexComp != 0)
            return vertexComp;
        return edge.compareTo(other.edge);
    }

    @Override
    public int hashCode() {
        return toString().hashCode(); // used to create the key and group values
    }

    @Override
    public String toString() {
        return String.format("%d:%d", vertex.get(), edge.get());
    }
}
