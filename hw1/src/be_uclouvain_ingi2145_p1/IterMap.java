package be_uclouvain_ingi2145_p1;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * Map by using each:
 * - edge for the key and the new rank as the value
 * - vertex for the key with all info (rank, edges)
 */
public class IterMap extends
        Mapper<LongWritable, Text, LongWritable, IterGenericClass> {

    @Override
    protected void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException {
        /*
         * We receive something like that:
         *
         * #Vertex Rank #edge1,#edge2,#edge3
         */
        String[] entries = value.toString().split(SocialRankDriver.SEPSTRING);
        Long vertex = Long.parseLong(entries[0]);
        Double rank = Double.parseDouble(entries[1]);
        Text edgesText;

        // Each edge: If this vertex has edges from itself
        if (entries.length > 2) {
            String edgesString = entries[2];
            LongWritable[] edges = VertexInfoWritable.getEdges(edgesString);
            edgesText = new Text(edgesString);

            DoubleWritable newRank = new DoubleWritable(rank / edges.length);
            IterGenericClass newRankGen = new IterGenericClass(newRank);

            // For each edge, share the new rank (influence/contribution)
            for (LongWritable edge : edges) {
                context.write(edge, newRankGen);
            }
        }
        else
            edgesText = new Text();

        // Each vertex:
        /*
         * Note: we could also send the previous rank in a VertexInfoWritable
         * object and then, we will be able to compute the diff directly instead
         * of doing that in an external job
         */
        IterGenericClass edgesGen = new IterGenericClass(edgesText);
        context.write(new LongWritable(vertex), edgesGen);
    }
}
