package be_uclouvain_ingi2145_p1;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * Count the number of values linked to our key
 */
public class ReciprocityVertexReduce
        extends
        Reducer<ReciprocityWritable, NullWritable, ReciprocityWritable, IntWritable> {

    @Override
    protected void reduce(ReciprocityWritable key,
            Iterable<NullWritable> values,
            Context context) throws IOException, InterruptedException {

        int count = 0;
        Iterator<NullWritable> iterator = values.iterator();
        while (iterator.hasNext()) {
            iterator.next(); // consume iterator
            count++;
        }

        context.write(key, new IntWritable(count));
    }
}
